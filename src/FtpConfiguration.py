class FtpConfiguration:
    def __init__(self, host: str, user_name: str, password: str, source_directory: str):
        self._host = None
        self._user_name = None
        self._password = None
        self._source_directory = None
        self.host = host
        self.user_name = user_name
        self.password = password
        self.source_directory = source_directory

    @property
    def host(self) -> str:
        return self._host

    @property
    def user_name(self) -> str:
        return self._user_name

    @property
    def password(self) -> str:
        return self._password

    @property
    def source_directory(self) -> str:
        return self._source_directory

    @host.setter
    def host(self, host: str):
        if host is None or host == "":
            raise ValueError("Empty FTP host")
        if not host.startswith("ftp"):
            raise ValueError("FTP host does not start with 'ftp'")
        self._host = host

    @user_name.setter
    def user_name(self, user_name: str):
        if user_name is None or user_name == "":
            raise ValueError("Empty FTP user name")
        self._user_name = user_name

    @password.setter
    def password(self, password: str):
        if password is None or password == "":
            raise ValueError("Empty FTP password")
        self._password = password

    @source_directory.setter
    def source_directory(self, source_directory: str):
        self._source_directory = source_directory
