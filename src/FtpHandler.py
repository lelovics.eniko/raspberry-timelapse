import datetime
from ftplib import FTP
from pathlib import Path
from typing import List, Dict

import os

from .FtpConfiguration import FtpConfiguration


class FtpHandler:

    @staticmethod
    def filter_file_names_by_date(original_list: List[str], current_date: datetime.date) -> str:
        readable_date = current_date.strftime('%Y%m%d')
        for elem in original_list:
            if readable_date in elem:
                yield elem

    @staticmethod
    def create_folder_structure(files: List[str]) -> Dict[str, List[str]]:
        file_name_dict: Dict[str, List[str]] = {}
        for file in files:
            if len(file.split("_")) >= 3:
                day = file.split("_")[2][0:8]
                current_date_files = file_name_dict.get(day, [])
                current_date_files.append(file)
                file_name_dict[day] = current_date_files
        return file_name_dict

    def __init__(self, configuration: FtpConfiguration, current_date: datetime.date):
        self.configuration = configuration
        self.current_date = current_date

    class open_ftp:

        def __init__(self, configuration: FtpConfiguration):
            self.configuration = configuration

        def __enter__(self) -> FTP:
            ftp = FTP(self.configuration.host)
            ftp.login(self.configuration.user_name, self.configuration.password)
            if self.configuration.source_directory:
                ftp.cwd(self.configuration.source_directory)
            self.ftp = ftp
            return ftp

        def __exit__(self, type, value, traceback):
            self.ftp.close()

    def save_files(self, image_directory_path: Path):
        with self.open_ftp(self.configuration) as ftp:
            list_of_images: Dict[str, List[str]] = FtpHandler.create_folder_structure(
                files=ftp.nlst()
            )
            for day in list_of_images.keys():
                os.mkdir(str(image_directory_path / day))
                for image in list_of_images[day]:
                    ftp.retrbinary(
                        "RETR " + image,
                        open(image_directory_path / day / image, 'wb').write)
