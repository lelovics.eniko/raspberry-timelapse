import os
import shutil


class FileHandler:

    @staticmethod
    def clear_directory(directory_path):
        for file in os.listdir(directory_path):
            file_path = directory_path / file
            try:
                if os.path.isfile(file_path):
                    os.unlink(file_path)
                else:
                    shutil.rmtree(file_path, ignore_errors=True)
            except Exception as e:
                print(e)
