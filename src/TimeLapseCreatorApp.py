import decouple
import datetime
from decouple import config

from src.FileHandler import FileHandler
from src.FtpConfiguration import FtpConfiguration
from src.FtpHandler import FtpHandler
from src.FilePathConfiguration import FilePathConfiguration


class TimeLapseCreatorApp:

    @staticmethod
    def start_process():
        try:
            app = TimeLapseCreatorApp()
            app.load_configuration()
            app.set_date_of_video()
            app.clear_images_directory()
            app.save_images_from_ftp()
        except Exception as err:
            print(err)

    def __init__(self):
        self.ftp_configuration = None
        self.are_images_from_today = None
        self.file_path_configuration = None
        self.date = None
        self.start_time = datetime.datetime.now()

    def load_configuration(self):
        try:
            self.ftp_configuration = FtpConfiguration(
                host=config('FTP_HOST'),
                user_name=config('FTP_USER_NAME'),
                password=config('FTP_PASSWORD'),
                source_directory=config('FTP_SOURCE_DIRECTORY')
            )
            self.file_path_configuration = FilePathConfiguration(
                image_directory_path='/images',
                video_directory_path='/videos'
            )
            self.are_images_from_today = config('ARE_IMAGES_FROM_TODAY', default=True)
        except decouple.UndefinedValueError as e:
            print("Missing parameter from .env file:")
            print(e)
            exit(1)
        except ValueError as e:
            print("Illegal value of a parameter:")
            print(e)
            exit(1)

    def set_date_of_video(self):
        self.date = datetime.date(
            year=self.start_time.year,
            month=self.start_time.month,
            day=self.start_time.day
        )
        if not self.are_images_from_today:
            self.date -= datetime.timedelta(days=1)

    def save_images_from_ftp(self):
        ftp_handler = FtpHandler(
            configuration=self.ftp_configuration,
            current_date=self.date
        )
        ftp_handler.save_files(self.file_path_configuration.image_directory_path)

    def clear_images_directory(self):
        FileHandler.clear_directory(self.file_path_configuration.image_directory_path)