from pathlib import Path


class FilePathConfiguration:

    def __init__(self, image_directory_path: str, video_directory_path: str):
        self._image_directory_path = None
        self._video_directory_path = None
        self.image_directory_path = image_directory_path
        self.video_directory_path = video_directory_path

    @property
    def image_directory_path(self) -> Path:
        return self._image_directory_path

    @property
    def video_directory_path(self) -> Path:
        return self._image_directory_path

    @image_directory_path.setter
    def image_directory_path(self, image_directory_path: str):
        if not image_directory_path:
            raise ValueError("Empty image directory path")
        if type(image_directory_path == str):
            image_directory_path = Path(image_directory_path)
        if not image_directory_path.exists():
            raise ValueError("Path does not exist: {0}".format(image_directory_path))
        self._image_directory_path = image_directory_path

    @video_directory_path.setter
    def video_directory_path(self, video_directory_path: str):
        if video_directory_path is None or video_directory_path == "":
            raise ValueError("Empty video directory path")
        if type(video_directory_path == str):
            video_directory_path = Path(video_directory_path)
        if not video_directory_path.exists():
            raise ValueError("Path does not exist: {0}".format(video_directory_path))
        self._video_directory_path = video_directory_path
